# script for gnuplot
set terminal postscript eps enhanced color "Helvetica" 20

stats 'issues.data' u 2 noout

set out "issues.ps"

ang(x)=x*360.0/STATS_sum        # get angle (grades)
perc(x)=x*100.0/STATS_sum       # get percentage

set size square                 # square canvas
set xrange [-1:1.5]
set yrange [-1.25:1.25]
set style fill solid 1

unset border
unset tics
unset key

Ai = 0.0; Bi = 0.0;             # init angle
mid = 0.0;                      # mid angle
i = 0; j = 0;                   # color
yi  = 0.8; yi2 = 0.8;           # label position

plot 'issues.data' u (0):(0):(1):(Ai):(Ai=Ai+ang($2)):(i=i+1) with circle linecolor var,\
     'issues.data' u (1.4):(yi=yi-0.5/STATS_records):($1) w labels left,\
     'issues.data' u (1.3):(yi2=yi2-0.5/STATS_records):(j=j+1) w p pt 5 ps 2 linecolor var,\
     'issues.data' u (mid=Bi+ang($2)*pi/360.0, Bi=2.0*mid-Bi, 1.3*cos(mid)):(1.15*sin(mid)):(sprintf('%.0f (%.1f\%)', $2, perc($2))) w labels

set terminal postscript eps enhanced color "Helvetica" 24

set out "issues-big.ps"

Ai = 0.0; Bi = 0.0;             # init angle
mid = 0.0;                      # mid angle
i = 0; j = 0;                   # color
yi  = 0.8; yi2 = 0.8;           # label position

plot 'issues.data' u (0):(0):(1):(Ai):(Ai=Ai+ang($2)):(i=i+1) with circle linecolor var,\
     'issues.data' u (1.4):(yi=yi-0.5/STATS_records):($1) w labels left,\
     'issues.data' u (1.3):(yi2=yi2-0.5/STATS_records):(j=j+1) w p pt 5 ps 2 linecolor var,\
     'issues.data' u (mid=Bi+ang($2)*pi/360.0, Bi=2.0*mid-Bi, 1.3*cos(mid)):(1.15*sin(mid)):(sprintf('%.0f (%.1f\%)', $2, perc($2))) w labels

set out "fixes-big.ps"

ang(x)=x*360.0/23        # get angle (grades)
perc(x)=x*100.0/23       # get percentage

Ai = 0.0; Bi = 0.0;             # init angle
mid = 0.0;                      # mid angle
i = 0; j = 0;                   # color
yi  = 0.8; yi2 = 0.8;           # label position

set xrange [-1.275:1.5]

plot 'fixes.data' u (0):(0):(1):(Ai):(Ai=Ai+ang($2)):(i=i+1) with circle linecolor var,\
     'fixes.data' u (1.4):(yi=yi-0.5/STATS_records):($1) w labels left,\
     'fixes.data' u (1.3):(yi2=yi2-0.5/STATS_records):(j=j+1) w p pt 5 ps 2 linecolor var,\
     'fixes.data' u (mid=Bi+ang($2)*pi/360.0, Bi=2.0*mid-Bi, 1.3*cos(mid)):(1.15*sin(mid)):(sprintf('%.0f (%.1f\%)', $2, perc($2))) w labels


reset
set terminal postscript eps enhanced color "Helvetica" 20

set out "fp.ps"

stats 'fp.data' u 2 noout

ang(x)=x*360.0/STATS_sum        # get angle (grades)
perc(x)=x*100.0/STATS_sum       # get percentage

set size square                 # square canvas
set xrange [-1:1.5]
set yrange [-1.25:1.25]
set style fill solid 1

unset border
unset tics
unset key

Ai = 0.0; Bi = 0.0;             # init angle
mid = 0.0;                      # mid angle
i = 0; j = 0;                   # color
yi  = 0.8; yi2 = 0.8;           # label position

plot 'fp.data' u (0):(0):(1):(Ai):(Ai=Ai+ang($2)):(i=i+1) with circle linecolor var,\
     'fp.data' u (1.4):(yi=yi-0.5/STATS_records):($1) w labels left,\
     'fp.data' u (1.3):(yi2=yi2-0.5/STATS_records):(j=j+1) w p pt 5 ps 2 linecolor var,\
     'fp.data' u (mid=Bi+ang($2)*pi/360.0, Bi=2.0*mid-Bi, 1.3*cos(mid)):(1.15*sin(mid)):(sprintf('%.0f (%.1f\%)', $2, perc($2))) w labels


reset

set terminal postscript eps enhanced color "Helvetica" 20

set out "issues_tfp.ps"

set key autotitle
set grid ytics
set auto x
unset xtics
set xtics nomirror
# rotate by -45 scale 0
set style data histogram
set style histogram rowstacked
set style fill solid 0.8 border -1
set boxwidth 0.75
set xrange [-0.75:2.75]
set ylabel "Number of issues"

plot "issues-tfpu.data" using 2:xtic(1) t column(2), \
	"issues-tfpu.data" using 3 title column(3), \
	"issues-tfpu.data" using 4 title column(4) lt 5

set terminal postscript eps enhanced color "Helvetica" 30
set out "issues_tfp-big.ps"
set key tmargin

plot "issues-tfpu-big.data" using 2:xtic(1) t column(2), \
	"issues-tfpu-big.data" using 3 title column(3), \
	"issues-tfpu-big.data" using 4 title column(4) lt 5
