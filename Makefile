#!/usr/bin/make -f

.PHONY: all ps pdf clean pics runclean

# --- programs
SHELL=bash
TEX=latex
PDFTEX=pdflatex
DVIPS=dvips
LYX=/Applications/LyX.app/Contents/MacOS/lyx
#LYX=lyx

# --- files

FILENAME=classification
DIRNAME=vst-2016

# ---------------------------------------------------------------------

all:	pdf

ps:	$(FILENAME).ps

pdf:	$(FILENAME).pdf

# --- generic rules

%.tex: %.lyx
	rm -f $(FILENAME).tex
	$(LYX) -e latex $*.lyx
	$(LYX) -e pdflatex $*.lyx

%.dvi: %.tex biblio.bib pics
	-$(TEX) $*.tex
	bibtex $*
	$(TEX) $*.tex
	$(TEX) $*.tex
	$(TEX) $*.tex

%.ps:	%.dvi
	$(DVIPS) -f $*.dvi >$@	

%.pdf:	%.tex biblio.bib pics
	-$(PDFTEX) $*.tex
	bibtex $* 
	$(PDFTEX) $*.tex
	$(PDFTEX) $*.tex
	$(PDFTEX) $*.tex

# --- specific rules

pics:
#	make -C pics
	make -C plot

zip: pics pdf
	rm -f $(FILENAME).zip
	zip $(FILENAME) \
	*.bib \
	$(FILENAME).{tex,dvi,pdf} \
	Makefile README \
	plot/*.{eps,pdf} \

tar: pics pdf
	cd ..; \
	tar --exclude \
	-cvzf $(FILENAME).tar.gz \
	$(DIRNAME)/*.bib \
	$(DIRNAME)/$(FILENAME).{tex,dvi,pdf} \
	$(DIRNAME)/{Makefile,README} \
	$(DIRNAME)/plot/*.{eps,pdf} \
	cd $(DIRNAME)

runclean:
	rm -f $(FILENAME).log $(FILENAME).aux $(FILENAME).toc *.blg *.bbl *.out

# add figclean below once there are any figures
clean:	runclean
	rm -f $(FILENAME).dvi $(FILENAME).ps $(FILENAME).pdf
	make -C plot clean
#	make -C pics clean
#	make -C plot clean

#distclean: clean
#	rm -f $(FILENAME).tex
